@extends('layouts.admin')
@section('content')
<h1>{{ $survey->survey_name }}</h1>
<h4>{{ $survey->survey_description }}</h4>
@csrf
<div class="table-responsive-xl">
<table class=" table-hover  p-5">
  <thead class="thead-dark">
    {{-- <tr>
        <th data-field="id" >Question</th>
        <th data-field="name" colspan="2">Answer(s)</th>
    </tr> --}}
    <tr>
        <th scope="col">User</th>
        <th scope="col">Code</th>
        <th scope="col">Date Create</th>
        @forelse ($survey->questions as $item)
        <th scope="col">{{ $item->question_title }}</th>
        @endforeach
        
    </tr>
  </thead>

  <tbody>
    
    <?php $count=false ?>
    {{-- @forelse ($survey->questions as $item) --}}
    {{-- {{dd($survey->questions)}} --}}
      @forelse ($survey->answers as $answer)
      <tr>
      {{-- {{dd($survey->questions)}} --}}
      {{-- {{dd($survey->answers)}} --}}
      {{-- {{dd($item->answers)}} --}}
      {{-- {{dd($item->question_type)}} --}}
        {{-- @forelse ($survey->questions as $item)
        <th>{{ $item->question_title }}</th>
        @endforeach --}}
        {{-- {{dd($answer->answer_content)}} --}}
        {{-- @if(!$count) --}}
            <td>{{$answer->users['last_name']}} {{$answer->users['first_name']}}</td>
            <td>{{$answer->users['code']}}</td>
            <td>{{$answer->created_at}}</td>
            <?php $count=true ?>
        {{-- @endif --}}
         
        <?php $content=json_decode($answer->answer_content,true); ?>
          @foreach ($content as $item=>$value)
            @if(count($value)>1)
            <td>
            @foreach ($value as $item1=>$value1)
              {{$value1}}<br>
            @endforeach
            </td>

            @else 
            @foreach ($value as $item1=>$value1)
              <td>{{$value1}}</td>
            @endforeach
            @endif
          @endforeach
          
        {{-- @endif --}}
        
        {{-- {{dd($contents)}} --}}
        

      {{-- @endforeach --}}
    @empty
      <tr>
        <td>
          No answers provided by you for this Survey
        </td>
        <td></td>
      </tr>
      
    @endforelse
    
  </tr>
  </tbody>
</table>
</div>
@endsection
