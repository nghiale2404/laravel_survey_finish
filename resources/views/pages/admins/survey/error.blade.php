@extends('layouts.admin')
@section('content')
    <div class="row">
            @if($message = Session::get('error'))
            <div class="alert alert-danger" role="alert">
                <p>{{$message}}</p>
                <p class="mb-0"></p>
            </div>
            @endif

@endsection
@section('script')
    <script>
        
    </script>
@endsection