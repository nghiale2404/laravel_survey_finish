@extends('layouts.admin')
@section('content')
  <div class="card p-5">
      <div class="card-content">
      <span class="card-title"> Start taking Survey</span>
      <h3 class="flow-text">{{ $survey->survey_name }}</h3><br/>
      <h4>{{ $survey->survey_description }}</h4><br/>
      Created by: <a href="">{{ $survey->users->last_name }} {{ $survey->users->first_name }}</a>
      
      {{-- {!! Form::open(array('action'=>array('AnswerController@store', $survey->survey_id))) !!} --}}
      <form method="POST" action="{{route('survey.complete',$survey)}}" id="boolean">
        @csrf
        <br>
          @forelse ($survey->questions as  $question)
          {{-- {{dd($survey->sqids)}} --}}
            <br><p class="flow-text"> {{ $question->question_title }}</p>
            {{-- {{dd($question)}} --}}
            {{-- {{dd($question->question_id)}} --}}
                @if($question->question_type === 'Text')
                  <div class="input-field col s12">
                    <input id="answer" type="text" name="{{$question->question_id}}[answer]">
                    <label for="answer">Answer</label>
                  </div>

                @elseif($question->question_type === 'Textarea')
                  <div class="input-field col s12">
                    <textarea id="textarea" class="materialize-textarea" name="{{$question->question_id}}[answer]"></textarea>
                    <label for="textarea">Textarea</label>
                  </div>

                @elseif($question->question_type === 'Radio')
                <?php $option=json_decode($question->question_option,true); ?>
        @foreach ($option as $item=>$value)
          @if(is_array($value))
            @foreach ($value as $item1=>$value1)
              <input name=" answer[]" type="Checkbox" id="{{ $item1 }}" value="{{$value1}}"/>
              <label for="{{ $item1 }}">{{ $value1 }}</label> <br>
            @endforeach
          @endif
        @endforeach

                @elseif($question->question_type === 'Checkbox')
          <?php $option=json_decode($question->question_option,true); ?>
            @foreach ($option as $item=>$value)
              @if(is_array($value))
                @foreach ($value as $item1=>$value1)
                  <input name="{{$question->question_id}}[] " type="Checkbox" id="{{ $item1 }}" value="{{$value1}}"/>
                  <label for="{{ $item1 }}">{{ $value1 }}</label> <br>
                @endforeach
              @endif
            @endforeach
                @endif
          @empty
            <span class='flow-text center-align'>Nothing to show</span>
          @endforelse
          <div class="form-group">
              <button type="submit" class="btn btn-success">submit</button>
          </div>
@endsection