@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-sm-6">
        @if($message = Session::get('success'))
            <div class="alert alert-success" role="alert">
            <p>{{$message}}</p>
            <p class="mb-0"></p>
            </div>
        @endif
        <div class="white-box">
            <form action="{{route('alumnies.update',$user_id)}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="code" class="control-label">Code</label>
                    <input type="text" class="form-control" id="code" name="code" value="{{$alumni->code}}" placeholder="Code">
                </div>
                <div class="form-group">
                    <label for="first_name" class="control-label">First Name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{$alumni->first_name}}" placeholder="First Name">
                </div>
                <div class="form-group">
                    <label for="last_name" class="control-label">Last Name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{$alumni->last_name}}" placeholder="Last Name">
                </div>
                <div class="form-group">
                    <label for="username" class="control-label">UserName</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{$alumni->username}}" placeholder="UserName">
                </div>
                <div class="form-group">
                    <label for="password" class="control-label">Password</label>
                    <input type="text" class="form-control" id="password" name="password" value="{{$alumni->password}}" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="nation" class="control-label">Nation</label>
                    <input type="text" class="form-control" id="nation" name="nation" value="{{$alumni->nation}}" placeholder="Password">
                </div>  
                <div class="form-group">
                    <label for="tel" class="control-label">Phone</label>
                    <input type="text" class="form-control" id="tel" name="tel" value="{{$alumni->tel}}" placeholder="Phone">
                </div>
                <div class="form-group">
                    <label for="email" class="control-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{$alumni->email}}" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="gender" class="control-label">Gender</label>
                    <input type="text" class="form-control" id="gender" name="gender" value="{{$alumni->gender}}" placeholder="Gender">
                </div>
                <div class="form-group">
                    <label for="birthday" class="control-label">Birthday</label>
                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{$alumni->birthday}}" placeholder="Birthday">
                </div>
                <div class="form-group">
                    <label for="address" class="control-label">Address</label>
                    <input type="text" class="form-control" id="address" name="address" value="{{$alumni->address}}" placeholder="Address">
                </div>
                <div class="form-group">
                    <label for="family_tel" class="control-label">Family Phone</label>
                    <input type="text" class="form-control" id="family_tel" name="family_tel" value="{{$alumni->family_tel}}" placeholder="Address">
                </div>
                <div class="form-group">
                    <label for="family_address" class="control-label">Family Adress</label>
                    <input type="text" class="form-control" id="family_address" name="family_address" value="{{$alumni->family_address}}" placeholder="Address">
                </div>
                <div class="form-group">
                    <label for="status_id" class="control-label">Status</label>
                    <input type="text" class="form-control" id="status_id" name="status_id" value="{{$alumni->status_id}}" placeholder="Status ID">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <a href="{{route('alumnies.index')}}" class="btn btn-default">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>
    
@endsection