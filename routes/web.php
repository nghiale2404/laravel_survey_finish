<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.admins.category.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// ACADEMY
Route::prefix('khoa-vien')->group(function () {
    Route::get('/', "Master\AcademyController@index")->name('khoa-vien/index');
    // ADD
    Route::get('/them', "Master\AcademyController@create_render")->name('khoa-vien/them');
    Route::post('/them', "Master\AcademyController@create_submit")->name('khoa-vien/them/submit');
    // EDIT
    Route::get('/edit/{academy_id}', 'Master\AcademyController@edit')->name('khoa-vien/edit');
    Route::post('/edit/submit/{academy_id}', 'Master\AcademyController@update')->name('khoa-vien/edit/submit');
    // DELETE
    Route::post('/destroy/{academy_id}', 'Master\AcademyController@destroy')->name('khoa-vien/destroy');
});

// STUDENT
Route::prefix('students')->group(function () {
    Route::get('/', 'Master\StudentController@index')->name('students.index');
    //IMPORT EXPORT
    Route::get('export', 'Master\StudentController@export')->name('students.export');
    Route::get('importExportView', 'Master\StudentController@importExportView');
    Route::post('import', 'Master\StudentController@import')->name('students.import');
    // ADD
    Route::get('/create', 'Master\StudentController@create')->name('students.create');
    Route::post('/create_submit', 'Master\StudentController@store')->name('students.store');
    // EDIT
    Route::get('/edit/{user_id}', 'Master\StudentController@edit')->name('students.edit');
    Route::post('/edit_submit/{user_id}', 'Master\StudentController@update')->name('students.update');
    // SHOW
    Route::get('/show/{user_id}', 'Master\StudentController@show')->name('students.show');
    // DELETE
    Route::post('/delete/{user_id}', 'Master\StudentController@destroy')->name('students.destroy');
});
// TEST
Route::get('/test', 'Master\StudentController@test');

// ALUMNI
Route::prefix('alumnies')->group(function () {
    Route::get('/', 'Master\AlumniController@index')->name('alumnies.index');

    // ADD
    Route::get('/create','Master\AlumniController@create')->name('alumnies.create');
    Route::post('/create_submit','Master\AlumniController@store')->name('alumnies.store');
    // EDIT
    Route::get('/edit/{user_id}','Master\AlumniController@edit')->name('alumnies.edit');
    Route::post('/edit_submit/{user_id}','Master\AlumniController@update')->name('alumnies.update');
    // SHOW
    Route::get('/show/{user_id}','Master\AlumniController@show')->name('alumnies.show');
    Route::post('/show/show_submit/{user_id}','Master\AlumniController@show_submit')->name('alumnies.show_submit');
    // DELETE
    Route::post('/delete/{user_id}','Master\AlumniController@destroy')->name('alumnies.destroy');

    // EXPORT
    Route::get('/export','Master\AlumniController@export')->name('alumnies.export');
    // IMPORT
    Route::post('/import','Master\AlumniController@import')->name('alumnies.import');

    // IMPORT GRADUATE
    Route::post('/import_graduate','Master\AlumniController@import_graduate')->name('alumnies.import_register_graduate');
    
    
});

// POST
Route::prefix('posts')->group(function () {
    Route::get('/', 'Master\PostController@index')->name('posts.index');
    Route::post('/','Master\PostController@store')->name('posts.store');

    // Login
    Route::get('/login','Master\PostController@login');
    Route::post('/checklogin','Master\PostController@checklogin')->name('posts.checklogin');
    // // ADD
    // Route::get('/create','Master\StudentController@create')->name('students.create');
    // Route::post('/create_submit','Master\StudentController@store')->name('students.store');
    // // EDIT
    // Route::get('/edit/{user_id}','Master\StudentController@edit')->name('students.edit');
    // Route::post('/edit_submit/{user_id}','Master\StudentController@update')->name('students.update');
    // // SHOW
    // Route::get('/show/{user_id}','Master\StudentController@show')->name('students.show');
    // // DELETE
    // Route::post('/delete/{user_id}','Master\StudentController@destroy')->name('students.destroy');
});

//Major
Route::group(['prefix' => 'major'], function () {
    Route::get('/', "Master\MajorController@index")->name('major/index');
    // ADD
    Route::get('/create', "Master\MajorController@create_render")->name('major/create');
    Route::post('/create', "Master\MajorController@create_submit")->name('major/create/submit');
    // EDIT
    Route::get('/edit/{major_id}', 'Master\MajorController@edit')->name('major/edit');
    Route::post('/edit/submit/{major_id}', 'Master\MajorController@update')->name('major/edit/submit');
    // DELETE
    Route::post('/destroy/{major_id}', 'Master\MajorController@destroy')->name('major/destroy');
});
//Major Branch
Route::group(['prefix' => 'major_branch'], function () {
    Route::get('/', "Master\MajorBranchController@index")->name('major_branch/index');
    // ADD
    Route::get('/create', "Master\MajorBranchController@create_render")->name('major_branch/create');
    Route::post('/create', "Master\MajorBranchController@create_submit")->name('major_branch/create/submit');
    // EDIT
    Route::get('/edit/{major_branch_id}', 'Master\MajorBranchController@edit')->name('major_branch/edit');
    Route::post('/edit/submit/{major_branch_id}', 'Master\MajorBranchController@update')->name('major_branch/edit/submit');
    // DELETE
    Route::post('/destroy/{major_branch_id}', 'Master\MajorBranchController@destroy')->name('major_branch/destroy');
});
//Class
Route::group(['prefix' => 'class'], function () {
    Route::get('/', "Master\ClassController@index")->name('class/index');
    // ADD
    Route::get('/create', "Master\ClassController@create_render")->name('class/create');
    Route::post('/create', "Master\ClassController@create_submit")->name('class/create/submit');
    // EDIT
    Route::get('/edit/{class_id}', 'Master\ClassController@edit')->name('class/edit');
    Route::post('/edit/submit/{class_id}', 'Master\ClassController@update')->name('class/edit/submit');
    // SHOW
    Route::get('/show/{class_id}', 'Master\ClassController@show')->name('class/show');
    // DELETE
    Route::post('/destroy/{class_id}', 'Master\ClassController@destroy')->name('class/destroy');
});

// // QUESTIONS

// route::prefix('questions')->group(function(){
//     route::get('/','Master\QuestionController@index')->name('questions/index');
//     //them
//     route::get('/create','Master\QuestionController@create_render')->name('questions/create');
//     route::post('/create','Master\QuestionController@create_submit')->name('questions/create/submit');
//     //edit
//     route::get('/edit/{question_id}','Master\QuestionController@edit');
//     route::post('/edit/submit/{question_id}','Master\QuestionController@update');
//     // delete
//     route::post('/destroy/{question_id}','Master\QuestionController@destroy')->name('questions/destroy');
// });

// //SURVEY_QUESTIONS

// route::prefix('surveyquestions')->group(function(){
//     route::get('/','Master\SurveyQuestionController@index')->name('surveyquestions/index');
//     //add
//     route::get('/create','Master\SurveyQuestionController@create_render')->name('surveyquestions/create');
//     route::post('/create/submit','Master\SurveyQuestionController@create_submit')->name('surveyquestions/create/submit');
//     //edit
//     route::get('/edit/{survey_question_id}','Master\SurveyQuestionController@edit');
//     route::post('/edit/submit/{survey_question_id}','Master\SurveyQuestionController@update');
//     //delete
//     route::post('/dectroy/{survey_question_id}','Master\SurveyQuestionController@destroy')->name('surveyquestions/destroy');

// });

// //SURVEYS

// route::prefix('surveys')->group(function(){
//     route::get('/','Master\SurveyController@index')->name('surveys/index');
//     //add
//     route::get('/create','Master\SurveyController@create_render')->name('surveys/create');
//     route::post('/create','Master\SurveyController@create_submit')->name('surveys/create/submit');
//     //edit
//     route::get('/edit/{survey_id}','Master\SurveyController@edit');
//     route::post('/edit/{survey_id}','Master\SurveyController@update');
//     //delete
//     route::post('/destroy/{survey_id}','Master\SurveyController@destroy')->name('surveys/destroy');

// });

// // ROLE_SURVEY

// route::prefix('rolesurveys')->group(function(){
//     route::get('/','Master\RoleSurveyController@index')->name('rolesurveys/index');

//     //add
//     route::get('/create','Master\RoleSurveyController@create_render')->name('rolesurveys/create');
//     route::post('/create','Master\RoleSurveyController@create_submit')->name('rolesurveys/create/submit');
//     //edit
//     route::get('/edit/{role_survey_id}','Master\RoleSurveyController@edit');
//     route::post('/edit/{role_survey_id}','Master\RoleSurveyController@update');
//     //delete
//     route::post('/destroy/{role_survey_id}','Master\RoleSurveyController@destroy')->name('rolesurveys/destroy');
// });

// CITY
Route::prefix('city')->group(function(){
    Route::get('/','Master\CityController@index')->name('city/index');
    //create
    Route::get('/create','Master\CityController@create_render')->name('city/create');
    Route::post('/create','Master\CityController@create_submit')->name('city/create/submit');
    //edit
    Route::get('/edit/{city_id}','Master\CityController@edit')->name('city/edit');;
    Route::post('/edit/submit/{city_id}','Master\CityController@update')->name('city/edit/submit');;
    // delete
    Route::post('/destroy/{city_id}','Master\CityController@destroy')->name('city/destroy');
});

// DISTRICT
Route::prefix('district')->group(function(){
    Route::get('/','Master\DistrictController@index')->name('district/index');
    //create
    Route::get('/create','Master\DistrictController@create_render')->name('district/create');
    Route::post('/create','Master\DistrictController@create_submit')->name('district/create/submit');
    //edit
    Route::get('/edit/{district_id}','Master\DistrictController@edit')->name('district/edit');;
    Route::post('/edit/submit/{district_id}','Master\DistrictController@update')->name('district/edit/submit');;
    // delete
    Route::post('/destroy/{district_id}','Master\DistrictController@destroy')->name('district/destroy');
});

// WARD
Route::prefix('ward')->group(function(){
    Route::get('/','Master\WardController@index')->name('ward/index');
    //create
    Route::get('/create','Master\WardController@create_render')->name('ward/create');
    Route::post('/create','Master\WardController@create_submit')->name('ward/create/submit');
    //edit
    Route::get('/edit/{ward_id}','Master\WardController@edit')->name('ward/edit');
    Route::post('/edit/submit/{ward_id}','Master\WardController@update')->name('ward/edit/submit');
    // delete
    Route::post('/destroy/{ward_id}','Master\WardController@destroy')->name('ward/destroy');
    // ajax create lấy district theo city_id
    Route::get('/district/{city_id}', 'Master\WardController@getDistrict');
});
Route::group(['prefix' => 'survey'], function (){
    Route::get('/', 'Master\SurveyController@index')->name('survey.index');
    //create survey
    Route::get('/create', 'Master\SurveyController@create_render')->name('survey.create_render');
    Route::post('/create_submit','Master\SurveyController@create_submit')->name('survey/create/submit');
    //add question
    Route::get('/{survey}', 'Master\SurveyController@detail_survey')->name('survey.detail');
    
    Route::post('/destroy/{survey_id}','Master\SurveyController@destroy')->name('survey/destroy');
    Route::get('/{survey}/edit', 'Master\SurveyController@edit')->name('survey.edit');
    Route::post('/{survey}/update', 'Master\SurveyController@update')->name('survey.update');

    Route::get('/view/{survey}', 'Master\SurveyController@view_survey')->name('survey.view');
    //xem đáp án
    Route::get('/answers/{survey}', 'Master\SurveyController@view_survey_answers')->name('view.survey.answers');

    Route::post('/view/{survey}/completed', 'Master\AnswerController@store')->name('survey.complete');
    Route::post('/create', 'Master\SurveyController@create')->name('survey.create');

    // Questions related
    Route::post('/{survey}/questions', 'Master\QuestionController@store')->name('survey.store');
});
Route::group(['prefix' => 'question'], function () {
    Route::get('/{question}/edit', 'Master\QuestionController@edit')->name('question.edit');
    Route::post('/{question}/update', 'Master\QuestionController@update')->name('question.update');
});
    
