<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('work_users')) {
            Schema::create('work_users', function (Blueprint $table) {
                $table->increments('work_user_id')->comment('id công việc của sinh viên');
                $table->integer('user_id')->unsigned()->comment('id người dùng');
                $table->integer('work_id')->unsigned()->comment('id công việc');
                $table->string('work_user_salary', 100)->comment('mức lương');
                $table->date('work_user_begin')->comment('ngày bất đầu vào làm việc');

                //log time
                $table->timestamp('created_at')
                    ->default(DB::raw('CURRENT_TIMESTAMP'))
                    ->comment('ngày tạo');

                $table->timestamp('updated_at')
                    ->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
                    ->comment('ngày cập nhật');

                $table->timestamp('deleted_at')
                    ->nullable()
                    ->comment('ngày xóa tạm');
            });
            DB::statement("ALTER TABLE `work_users` comment 'Thông tin cựu sinh viên đang làm việc.'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
