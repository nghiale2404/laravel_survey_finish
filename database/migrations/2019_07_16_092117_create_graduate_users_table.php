<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraduateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('graduate_users')){
            Schema::create('graduate_users', function (Blueprint $table) {
                $table->increments('graduate_users_id')->comment('id lưu trữ thông tin tốt nghiệp của người dùng');
                $table->integer('register_graduate_id')->comment('id mẫu đăng ký tốt nghiêp')->unsigned()->index();
                $table->integer('academy_id')->comment('id khoa/viên')->unsigned()->index();
                $table->integer('major_id')->comment('id nghành')->unsigned()->index();
                $table->integer('major_brach_id')->comment('id chuyên nghành')->unsigned()->index();
                $table->integer('class_id')->comment('id lớp')->unsigned()->index();
                $table->integer('user_id')->comment('id người dùng')->unsigned()->index();
                $table->string('register_graduate_type_of_tranning')->comment('hệ đào tạo');
                $table->text('register_graduate_note')->comment('ghi chú');
                
                $table->timestamp('created_at')
                    ->default(DB::raw('CURRENT_TIMESTAMP'))
                    ->comment('ngày tạo');
    
                    $table->timestamp('updated_at')
                        ->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
                        ->comment('ngày cập nhật');
    
                    $table->timestamp('deleted_at')
                        ->nullable()
                        ->comment('ngày xóa tạm');
            });
            DB::statement("ALTER TABLE `graduate_users` comment 'Lưu trữ liên kết thông tin tốt nghiệp và người dùng'");
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
