<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('user_id')->comment('id người dùng');
                // Mac dinh lenght = 10 , Khong dc comment
                $table->string('code', 12)->comment('ma sv hoac gv');
                $table->string('first_name')->commnet('ten dem va ten');
                $table->string('last_name')->commnet('ho');
                $table->char('username', 10)->commnet('ten dang nhap');
                $table->char('password', 60)->commnet('mat khau');
                $table->char('course', 5)->comment('khóa');
                $table->char('nation', 45)->comment('dân tộc');
                $table->string('tel', 20)->commnet('sdt');
                $table->string('email', 64)->commnet('email');
                $table->text('address')->comment('địa chỉ');
                $table->date('birth')->comment('ngày sinh');
                $table->string('gender')->comment('giới tính');
                $table->string('family_tel')->comment('số điện thoại gia đình');
                $table->string('family_address')->comment('địa chỉ gia đình');
                // $table->integer('status_id')->unsigned()->index()->comment('FK id trạng thái của người dùng');
                // $table->integer('ward_id')->unsigned()->index()->comment('FK id phường/xã');
                // $table->text('token')->commnet('token bao mat');
                
                
                // log time
                $table->timestamp('created_at')
                ->default(DB::raw('CURRENT_TIMESTAMP'))
                ->comment('ngày tạo');

                $table->timestamp('updated_at')
                    ->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
                    ->comment('ngày cập nhật');

                $table->timestamp('deleted_at')
                    ->nullable()
                    ->comment('ngày xóa tạm');
                // Setting unique
                $table->unique(['username','code']);
            });
            DB::statement("ALTER TABLE `users` comment 'Luu tru thong tin nguoi dung'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
