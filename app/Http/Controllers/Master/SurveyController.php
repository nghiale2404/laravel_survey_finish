<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use App\Http\Controllers\Controller;
use App\Models\Survey;
use App\Models\Question;
use App\Models\Answer;
use App\Models\User;
use Carbon\Carbon;

class SurveyController extends Controller{
	//khoi tao
	public function __construct(){
			parent::__construct();
	}
	public function home(Request $request) 
	  {
	    $surveys = Survey::get();
	    return view('home', compact('surveys'));
	  }
	public function index(Request $request)
	    {
	    $config = [
	      'model' => new Survey(),
	      'request' => $request,
	    ];
	    $this->config($config);
	    $survey = $this->model->web_index($this->request);

	    return view('pages.admins.survey.index', ['survey' => $survey]);
	    }
  # Show page to create new survey
  public function create_render() 
  {
    return view('pages.admins.survey.create');
  }

  public function create_submit(Request $request) 
  {
    //set ngày bắt đầu trước ngày kết thúc
    $this->validate($request,[
      'survey_start' => 'required|date|before:survey_end',
      'survey_end' => 'required|date|after:survey_start',
  ]);
    $config = [
            'model' => new Survey(),
            'request' => $request,
        ];
        $this->config($config);
        $data = $this->model->web_insert($this->request);
    return redirect("/survey/{$this->request->survey_id}");
  }

  public function detail_survey(Survey $survey) 
  {
    return view('pages.admins.survey.detail', compact('survey'));
  }
  

  public function edit(Survey $survey) 
  {
    $survey_id = Survey::findOrFail($survey->survey_id);
    
    return view('pages.admins.survey.edit',compact('survey','survey_id'));
  }

  public function view_survey(Survey $survey) 
  { 
    //set ngày khảo sát
    $day=Carbon::now()->day; //ngày
    $month=Carbon::now()->month; //tháng
    $year=Carbon::now()->year; //năm
    $start_day=Carbon::parse($survey->survey_start)->day;
    $start_month=Carbon::parse($survey->survey_start)->month;
    $start_year=Carbon::parse($survey->survey_start)->year;
    $end_day=Carbon::parse($survey->survey_end)->day;
    $end_month=Carbon::parse($survey->survey_end)->month;
    $end_year=Carbon::parse($survey->survey_end)->year;
    if($start_year<=$year && $year<=$end_year){
      if($start_month<=$month && $month<=$end_month){
        if($start_day<=$day && $day<=$end_day){
          return view('pages.admins.survey.view', compact('survey'));
        }
        //sau khi hoàn thành set lại trang back
        else return back()->with('error','Chưa đến thời gian khảo sát');
      }
      //sau khi hoàn thành set lại trang back
      else return back()->with('error','Chưa đến thời gian khảo sát');
    }
    //sau khi hoàn thành set lại trang back
    else return back()->with('error','Chưa đến thời gian khảo sát');
  }

  # view submitted answers from current logged in user
  public function view_survey_answers(Survey $survey) 
  {
    
    $survey->load('answers');

    return view('pages.admins.answer.view', compact('survey'));
  }

	public function update(request $request, $survey_id){
    $survey = Survey::find($survey_id);
    //lấy id người tạo
		$survey->user_id			=	'1';
		$survey->survey_name		=	$request->get('survey_name');
		$survey->survey_description	=	$request->get('survey_description');
		$survey->survey_start		=	$request->get('survey_start');
		$survey->survey_end		=	$request->get('survey_end');
		$survey->survey_version		=	$request->get('survey_version');

		$survey->save();

		return redirect('survey')->with('success','Updated Successfully!');
	}

	public function destroy ($survey_id){
		$data = Survey::findOrFail($survey_id);
		$data->delete();

		return redirect('survey')->with('success','Deleted Successfully!');
	}
}