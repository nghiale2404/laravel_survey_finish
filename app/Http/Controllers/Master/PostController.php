<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function login()
    {
        return view('pages.admins.login.login');
    }

    public function checklogin(Request $request)
    {
        $this->validate($request,[
            'username'  => 'required',
            'password'  => 'required|min:3'
        ]);

        $user_data = array(
            'username' => $request->get('username'),
            'password'  => $request->get('password'),
        );
        if(Auth::attempt($user_data))
        {
            return redirect('posts.index');
        }
        else{
            return \back()->with('error','Wrong Login Details');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.admins.posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'post_title'    => 'required',
            'post_content'  => 'required',
       ]);
       $posts = new Post([
            'user_id'   => Auth::user()->user_id,
            'class_id'  => $request->get('class_id'),
            'role_id'   => $request->get('role_id'),
            'post_title'   => $request->get('post_title'),
            'post_content'  => $request->get('post_content'),
            'post_slug'     => $request->get('post_slug'),
            'post_status'   => $request->get('post_status'),
       ]);
       $posts->save();
       return redirect('posts',\compact('posts',$posts));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
