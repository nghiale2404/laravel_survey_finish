<?php
	
namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Survey;
use App\Models\Question;
use App\Models\SurveyQuestion;
use DB;

class QuestionController extends Controller{

	// Ham khoi tao
	public function __construct(){
		parent::__construct();
	}

	// public function store(Request $request,Survey $survey) 
    // { 	
	// 	$dt='';
	// 	if($request->question_option){
	// 	foreach($request->question_option as $key => $value){
	// 		$dt= $dt.'/option'.$value;
	// 	}}
	// 	//sử dụng config
	// 	$question_id=Question::insert([
	// 		'survey_id'=>$survey->survey_id,
	// 		'question_title'=>$request->question_title,
	// 		'question_type'=>$request->question_type,
	// 		'question_option'=>$dt,
			
	// 	]);
	// 	return back();

		
	// }
	public function store(Request $request, Survey $survey) 
  	{
    // remove the token
    $arr = $request->except('_token');
    foreach ($arr as $key => $value) {
		// dd($key);
      $Question = new Question();
	if(count($arr)>2){
		$Option = json_encode($arr, true);
	}
	else $Option=null;

      $questionArray[] = $Question;
    };
      $Question->insert([
        'survey_id' => $survey->survey_id,
        'question_title'=>$request->question_title,
		'question_type'=>$request->question_type,
		'question_option'=>$Option,
      ]);
    return back()->with('success','Successful Survey!');
  	}

    public function edit(Question $question) 
    {
		$question_id = Question::findOrFail($question->question_id);

      return view('pages.admins.question.edit', compact('question','question_id'));
    }

    public function update(Request $request, $question_id) 
    {
		// dd($request);
		// var_dump($request->except);die;
			// if($request->question_option){
				$title=$request->question_title;
				$arr = $request->except('_token','question_title');
				// dd($arr);
				$Question = Question::findOrFail($question_id);
				foreach ($arr as $key => $value) {
					if (! is_array( $value )) {
					  $title = $value[$value];
					} else {
					  $Option = json_encode($arr, true);
					  // var_dump($decode);die;
					}
			
				$questionArray[] = $Question;
				}
				$Question->update([
					'question_title'=>$title,
					'question_option'=>$Option,
				]);
				
			
	  	return back()->with('success','Updated Successfully!');
	
	//   $question->update($request->all());
	  
    //   return redirect()->action('SurveyController@detail_survey', [$question->survey_id]);
    }


	// public function index(request $request){
	// 	$config = [
	// 		'model' => new Question(),
	// 		'request' => $request
	// 	];
	// 	$this->config($config);
	// 	$data = $this->model->web_index($this->request);
	// 	return view('pages.admins.question.index',['data'=>$data]);
	// }

	// public function create_render(request $request){
	// 	return view('pages.admins.question.create');
	// }

	// public function create_submit(request $request){
	// 	$config = [
	// 		'model' => new Question(),
	// 		'request' => $request
	// 	];
	// 	$this->config($config);
	// 	$data = $this->model->web_insert($this->request);
	// 	return redirect('questions')->with('success','Added Successfully');
	// }


	// public function edit($question_id){
	// 	$question = Question::findOrFail($question_id);
	// 	return view('pages.admins.question.edit',compact('question','question_id'));
	// }

	// public function update(request $request,$question_id){
	// 	$question =	 Question::find($question_id);

	// 	$question->question_content = $request->get('question_content');
	// 	$question->question_type	= $request->get('question_type');
	// 	$question->question_answer	= $request->get('question_answer');
  		
  	// 	$question->save();

  	// 	return redirect('questions')->with('success','Succsssfully');
	// }

	// public function destroy($question_id){
	// 	$data = Question::findOrFail($question_id);
	// 	$data->base_soft_delete();
	// 	return redirect('questions')->with('success','Successfully');	
	// }

}