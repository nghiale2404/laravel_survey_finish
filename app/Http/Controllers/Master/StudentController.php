<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Classes;
use App\Models\Major;
use App\Models\MajorBranch;
use App\Models\Academy;
use App\Models\Ward;
use App\Models\City;
use App\Models\District;
use App\Models\ClassUser;
use DB;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $config = [
            'model'  => new User(),
            'request'   => $request,
        ];
        $this->config($config);
        $students = $this->model->web_index($this->request);

        // Phân trang bị lỗi !
        // $students = User::latest()->get();

        // $users = User::all();
        // foreach($users as $user)
        // {
        //     echo $user->classes . '<br>';
        // }
        return view('pages.admins.students.index',['students' => $students]);
    }

    public function importExportView()
    {
        return view('import');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function export()
    {
        return Excel::download(new UsersExport(), 'users.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function import(Request $request)
    {
        $user = Excel::ToCollection(new UsersImport(), $request->file('file'));
        foreach ($user[0] as $user) {

            User::where('code', $user[0])->insert([
                'code' => $user[0],
                'first_name' => $user[1],
                'last_name' => $user[2],
                'username' => $user[3],
                'password' => Hash::make($user[4]),
                'course' => $user[5],
                'nation' => $user[6],
                'tel' => $user[7],
                'email' => $user[8],
                'address' => $user[9],
                'birth' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($user[10]),
                'gender' => $user[11],
                'family_tel' => '',
                'family_address' => '',
                'status' => '',
                'reason' => '',
            ]);
            $temp1=User::where('code',$user[0])->first();
            $temp2=Classes::where('class_code',$user[12])->first();
            ClassUser::insert([
                'user_id'=> $temp1->user_id,
                'class_id'=> $temp2->class_id,
                'class_user_accountability'=>'sinh viên',
            ]);

        }

        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admins.students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $config = [
            'model' => new User(),
            'request' => $request,
        ];
        $this->config($config);
        $student = $this->model->web_insert($this->request);
        $usercode=$this->request->code;
        $classcode=$this->request->class_code;
        $temp1=User::where('code',$usercode)->first();
        $temp2=Classes::where('class_code',$classcode)->first();
        ClassUser::insert([
            'user_id'=> $temp1->user_id,
            'class_id'=> $temp2->class_id,
            'class_user_accountability'=>'sinh viên',
        ]);

        // $config = [
        //     'model' => new ClassUser(),
        //     'request' => $request,
        // ];
        // $this->config($config);
        // $student = $this->model->web_insert($this->request);
        // dd($student);
        return redirect('students')->with('success', 'Added Data Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = User::find($user_id);
        $classuser = ClassUser::where('user_id', $user->user_id)->first();
        $class = Classes::where('class_id', $classuser->class_id)->first();
        $majorbranch = MajorBranch::where('major_branch_id', $class->major_branch_id)->first();
        $major = Major::where('major_id', $majorbranch->major_id)->first();
        $academy = Academy::where('academy_id', $major->academy_id)->first();
        // $birthday = $user->birth->isoFormat('dd/mm/YYYY');
        // $ward = Ward::where('ward_id', $user->ward)->first();
        // $district = District::where('district_id', $ward->district_id)->first();
        // $city = City::where('city_id', $ward->city_id)->first();
        return view('pages.admins.students.show', ['user' => $user, 'class' => $class, 'majorbranch' => $majorbranch, 'major' => $major, 'academy' => $academy]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);
        $classuser = ClassUser::where('user_id', $user->user_id)->first();
        $class = Classes::where('class_id', $classuser->class_id)->first();
        $majorbranch = MajorBranch::where('major_branch_id', $class->major_branch_id)->first();
        $major = Major::where('major_id', $majorbranch->major_id)->first();
        $academy = Academy::where('academy_id', $major->academy_id)->first();

        return view('pages.admins.students.edit', compact('user', 'user_id'),['user' => $user, 'class' => $class, 'majorbranch' => $majorbranch, 'major' => $major, 'academy' => $academy]);
        //,['students' => $students]
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        $this->validate($request, [
            'tel' => 'required',
            'email' => 'required',
            'address' => 'required',
            'family_tel' => 'required',
            'family_address' => 'required',
        ]);
        $user = User::find($user_id);
        //TODO:  Nhan du lieu tu form cu
        $user->tel = $request->get('tel');
        $user->email = $request->get('email');
        $user->address = $request->get('address');
        $user->family_tel = $request->get('family_tel');
        $user->family_address = $request->get('family_address');
        $user->save();

        return redirect('students')->with('success', 'Updated Data Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        $student = User::findOrFail($user_id);
        $student->delete();

        return redirect('students')->with('success', 'Deleted Successfully!');
    }
}
