<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use App\Models\Status;

// Excel
use App\Imports\AlumniImport;
use App\Exports\AlumniExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GraduateImport;

// use Maatwebsite\Excel\Facades\Excel;

class AlumniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // FIXME: https://viblo.asia/q/join-query-builder-trong-laravel-WrZn07pr5xw
        // FIXME: https://stackoverflow.com/questions/46846225/property-name-does-not-exist-on-this-collection-instance
        //$alumnies = User::latest()->get();
        
        //$alumnies = User::latest()->paginate(5);              // phân trang. Nhưng gộp lại thành $users luôn

        // Lấy ra trạng thái tương ứng với người dùng đó
        $users = User::with('statuses')->latest()->paginate(1);
        // dd($users);
        // echo $users;
        // foreach($users as $user)
        // {
        //     echo $user->statuses. '<br>';
        // }
        return view('pages.admins.alumnies.index',compact('users'))
        ->with('i',(request()->input('page',1) - 1 ) * 1)
        ->with('users',$users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admins.alumnies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $config = [
        //     'model' => new User(),
        //     'request'   => $request,
        // ];
        // $this->config($config);
        // $alumnies = $this->model->web_insert($this->request);

        $this->validate($request,[
            'code'          => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'username'      => 'required',
            'password'      => 'required|min:3',
            'nation'        => 'required',
            'tel'           => 'required',
            'email'         => 'required|email',
            'gender'        => 'required',
            'birthday'      => 'required',
            'address'       => 'required',
            'family_tel'    => 'required',
            'family_address'=> 'required',
            'status_id'     => 'required',
        ]);

        $status_id = '';
        $alumnies = new User();

        $alumnies->code             = $request->get('code');
        $alumnies->first_name       = $request->get('first_name');
        $alumnies->last_name        = $request->get('last_name');
        $alumnies->username         = $request->get('username');
        $alumnies->password         = $request->get('password');
        $alumnies->nation           = $request->get('nation');
        $alumnies->tel              = $request->get('tel');
        $alumnies->email            = $request->get('email');
        $alumnies->gender           = $request->get('gender');
        $alumnies->birthday         = $request->get('birthday');
        $alumnies->address          = $request->get('address');
        $alumnies->family_tel       = $request->get('family_tel');
        $alumnies->family_address   = $request->get('family_address');
        $alumnies->status_id        = $request->get('status_id');

        $alumnies->save();
        /****************************************************************************************
         * ***************************************************************************************
         *  Thêm dữ liệu bên bảng users sẽ thêm status_id bên bảng pivot status_users luôn. 
         * 
         * ***************************************************************************************/
        $alumnies->statuses()->attach($alumnies->status_id);
        return redirect('alumnies')->with('success','Add Data Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user_id
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$alumni_id)
    {
        //$alumnies = User::findOrFail($alumni_id);
        $alumnies = User::with('statuses')->get()->find($alumni_id);
        echo $alumnies->statuses . '<br>';
        //dd($alumnies);

        /****************************************************************************************
         * ***************************************************************************************
         * 2 biến để dổ dữ liệu của status_name và status_reason sang View alumnies/show.blade.php
         * 
         * ***************************************************************************************
         ****************************************************************************************/
        $status_name = '';
        $status_reason = '';
        foreach($alumnies->statuses as $status)
        {
            //return $status_name = $status->status_name. '<br>';
            $status_name = $status->status_name;
            //return $status_reason = $status->pivot->status_users_reason;
            $status_reason = $status->status_reason;
        }

        // Học kỳ tốt nghiệp
        $semester = '';
        // Năm tốt nghiệp
        $session = '';
        // Ngay tot nghiep
        $graduate_date = '';
        // Quyet dinh tot nghiep
        $graduate_decide = '';
        // GPA
        $GPA = '';
        // DRL
        $DRL ='';
        // TCTL
        $TCTL ='';
        // Bac tot nghiep
        $ranked = '';
        // Danh hieu
        $degree ='';

        $alumnies_graduates = User::findOrFail($alumni_id);
        $alumnies_graduates = User::with('graduates')->get()->where('user_id',$alumni_id);  // that's awesome !
        // echo $graduates;
        //dd($alumnies_graduates);
        foreach($alumnies_graduates as $alumnies_graduate)
        {
            //echo 'Diem TN:' .$alumnies_graduate->graduates;
            foreach ($alumnies_graduate->graduates as $graduate) {
                 //echo 'HOC KY: ' . $graduate->register_graduate_semester;
                $semester           = $graduate->register_graduate_semester;
                $session            = $graduate->register_graduate_session;
                $graduate_date      = $graduate->register_graduate_date;
                $graduate_decide    = $graduate->register_graduate_decide;
                $GPA                = $graduate->register_graduate_GPA;
                $DRL                = $graduate->register_graduate_DRL;
                $TCTL               = $graduate->register_graduate_TCTL;
                $ranked             = $graduate->register_graduate_ranked;
                $degree             = $graduate->register_graduate_degree;

            }
        }
        return view('pages.admins.alumnies.show',\compact('alumnies','alumni_id'))
                ->with('status_name',$status_name)
                ->with('status_reason',$status_reason)
                ->with('semester',$semester)
                ->with('session',$session)
                ->with('graduate_date',$graduate_date)
                ->with('graduate_decide',$graduate_decide)
                ->with('GPA',$GPA)
                ->with('DRL',$DRL)
                ->with('TCTL',$TCTL)
                ->with('ranked',$ranked)
                ->with('degree',$degree);
    }

    public function show_submit()
    {
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        $alumni = User::findOrFail($user_id);
        return view('pages.admins.alumnies.edit',compact('alumni','user_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        $this->validate($request,[
            'code'                  => 'required',
            'first_name'            => 'required',
            'last_name'             => 'required',
            'username'              => 'required',
            'password'              => 'required',
            'tel'                   => 'required',
            'email'                 => 'required',
            'gender'                => 'required',
            'birthday'              => 'required',
            'address'               => 'required',
            'status_id'             => 'required',
        ]);

        $alumni = User::findOrFail($user_id);
        $alumni->code                      = $request->get('code');
        $alumni->first_name                = $request->get('first_name');
        $alumni->last_name                 = $request->get('last_name');
        $alumni->username                  = $request->get('username');
        $alumni->password                  = $request->get('password');
        $alumni->tel                       = $request->get('tel');
        $alumni->email                     = $request->get('email');
        $alumni->gender                    = $request->get('gender');
        $alumni->birthday                  = $request->get('birthday');
        $alumni->address                   = $request->get('address');
        $alumni->status_id                 = $request->get('status_id');
        $alumni->save();
        return redirect('alumnies')->with('success','Updated Data Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        $alumni = User::findOrFail($user_id);
        $alumni->delete();
        return redirect('alumnies')->with('success','Data Deleted Success');
    }

    public function import()
    {
        Excel::import(new AlumniImport,request()->file('file'));
        return back();  

    }

    public function export()
    {
        return Excel::download(new AlumniExport,'alumnies.xlsx');
    }

    public function import_graduate()
    {
        Excel::import(new GraduateImport,request()->file('file'));
        return back();  
    }
}
