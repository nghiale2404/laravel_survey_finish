<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Survey;
use App\Models\SurveyQuestion;
use App\Models\Question;
use App\Models\Answer;
use App\Models\User;

class SurveyQuestionController extends Controller{
	//khoi tao
	public function __construct(){
			parent::__construct();
	}

  # view survey publicly and complete survey
  public function view_survey(SurveyQuestion $surveyquestion) 
  { 
    // $survey->survey_option = unserialize($survey->survey_option);
    // dd($survey);
    return view('pages.admins.survey.view', compact('surveyquestion'));
  }

  # view submitted answers from current logged in user
  public function view_survey_answers(SurveyQuestion $surveyquestion) 
  {
    $surveyquestion->load('surveys.questions.answers');
    // return view('survey.detail', compact('survey'));
    // return $survey;
    return view('pages.admins.answer.view', compact('surveyquestion'));
  }
  public function store(Request $request, SurveyQuestion $surveyquestion) 

}