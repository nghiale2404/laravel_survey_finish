<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\Survey;
use App\Models\User;
use App\Models\Answer;
use Auth;


class AnswerController extends Controller
{
  public function __construct()
  {
    parent::__construct();
  }

  public function store(Request $request, Survey $survey) 
  {
    // remove the token
    $arr = $request->except('_token');
    foreach ($arr as $key => $value) {
      $Answer = new Answer();

      if (! is_array( $value )) {
        $Value = $value[$key];
      } else {
        $Value = json_encode($arr, true);
      }

      $answerArray[] = $Answer;
    };
    $Answer->user_id = "1";
      $Answer->insert([
        'survey_id' => $survey->survey_id,
        'user_id'     =>"1",
        'answer_content'=>$Value,
      ]);
      
      // return ($decode);
    return back()->with('success','Successful Survey!');
  }
}
