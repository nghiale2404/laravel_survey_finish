<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'code' => $user[0],
            'first_name' => $user[1],
            'last_name' => $user[2],
            'username' => $user[3],
            'password' => Hash::make($user[4]),
            'course' => $user[5],
            'nation' => $user[6],
            'tel' => $user[7],
            'email' => $user[8],
            'address' => $user[9],
            'birth' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($user[10]),
            'gender' => $user[11],
            'family_tel' => '',
            'family_address' => '',
            'active_code' => $user[12],
            'status' => '',
            'reason' => '',
            'created_at'    =>'' ,
            'updated_at'     =>'',


        ]);
    }
}
