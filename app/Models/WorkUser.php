<?php

namespace App\Models;

use App\Models\Base\BaseModel;

class WorkUser extends BaseModel
{
    protected $table = 'work_users';

    protected $primaryKey = 'work_user_id';

    protected $keyType = 'int';

    protected $fillable = [
        'work_user_id',
        'work_id',
        'user_id',
        'work_user_salary',
        'work_user_begin',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = true;
}
