<?php

namespace App\Models;

use App\Models\Base\BaseModel;

class Role extends BaseModel
{
    protected $table = 'roles';

    protected $primaryKey = 'role_id';

    protected $keyType = 'int';

    protected $fillable = [
        'role_id',
        'role_name',
        'role_level',
        'role_note',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = true;
}
