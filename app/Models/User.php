<?php

namespace App\Models;

use App\Models\Base\BaseModel;
use Illuminate\Http\Request;
use App\Models\Answer;

class User extends BaseModel
{
    protected $table = 'users';

    protected $primaryKey = 'user_id';

    protected $keyType = 'int';

    protected $fillable = [
        'user_id',
        'code',
        'first_name',
        'last_name',
        'username',
        'password',
        'course',
        'nation',
        'tel',
        'email',
        'address',
        'birth',
        'gender',
        'family_tel',
        'family_address',
        // 'status_id',
        // 'ward_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = true;

    // 
    public function __construct()
    {
        $this->fillable_list = $this->fillable;
    }
    public function base_update(Request $request)
    {
        // $filter_param = $request->only($this->$fillable);
        $this->update_conditions = [
            'user_id' => 1
        ];
        return parent::base_update($this->request);
    }

    public function classes()
    {
        return $this->belongsToMany(Classes::class,'class_users','user_id','class_id');
    }

    // trạng thái của người dùng : ĐI hc , đi làm , nghỉ học
    // 3 bảng: users, statuses, pivot_table status_users
    public function statuses()
    {
        return $this->belongsToMany(Status::class,'status_users','user_id','status_id');
    }

    // 2 bảng: statuses, users
    // status_id trong bảng users,
    // public function status_index()
    // {
    //     return $this->belongsTo(Status::Class,'user_id','status_id');
    // }

    // Điểm tốt nghiệp trong function Show
    public function graduates()
    {
        return $this->belongsToMany(RegisterGraduate::class,'graduate_users','user_id','register_graduate_id');
    }

    // public function graduate_users()
    // {
    //     return $this->belongsToMany(GraduateUser::class,'graduate_users','user_id','user_id');
    // }
    // public function surveys()
    // {
    //     return $this->hasMany(Survey::class);
    // }
    
}